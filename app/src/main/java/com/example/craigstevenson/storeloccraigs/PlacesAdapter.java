package com.example.craigstevenson.storeloccraigs;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class PlacesAdapter extends BaseAdapter{

    private ArrayList<Place> places;
    private Context context;

    public PlacesAdapter(Context context, ArrayList<Place> places) {
        this.places = places;
        this.context = context;
    }

    @Override
    public int getCount() {
        return places.size();
    }

    @Override
    public Object getItem(int position) {
        return places.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_places_row, null);
        }

        TextView nameTextView = (TextView) convertView.findViewById(R.id.placeNameText);
        ImageView placeImageView = (ImageView) convertView.findViewById(R.id.mHistoryImage);
        TextView distanceTextView = (TextView) convertView.findViewById(R.id.placeDistanceText);

        int distance = places.get(position).getDistance();

        nameTextView.setText(places.get(position).getName());
        distanceTextView.setText(String.format("%,d", distance) + " m");
        placeImageView.setImageResource(R.drawable.places_icon);

        Bitmap thumbnail = places.get(position).getMainImage();

        if(thumbnail != null)
        {
            placeImageView.setImageBitmap(thumbnail);
        }

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
