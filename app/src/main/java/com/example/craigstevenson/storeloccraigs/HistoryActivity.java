package com.example.craigstevenson.storeloccraigs;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends ListActivity implements ShakeEventManager.ShakeListener{

    public PlacesAdapter placesAdapter;
    private ShakeEventManager sd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        sd = new ShakeEventManager();
        sd.setListener(this);
        sd.init(this);

        Place placeHelper = new Place(this);
        List<Place> listOfPlaces = placeHelper.getAll();

        ArrayList<Place> mlistOfPlaces = (ArrayList<Place>) placeHelper.getAll();
        placesAdapter = new PlacesAdapter(this, mlistOfPlaces);
        setListAdapter(placesAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Place place = (Place) placesAdapter.getItem(position);

        Intent intent = new Intent(this, SessionActivity.class);

        // pass the google placeID to the activity
        intent.putExtra("gPlaceID", place.getGooglePlaceID());
        startActivity(intent);

    }

    @Override
    public void onShake() {
        Intent intent = new Intent(this, MainActivity.class);
        finish(); // stop the current activity
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sd.register();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sd.deregister();
    }
}
