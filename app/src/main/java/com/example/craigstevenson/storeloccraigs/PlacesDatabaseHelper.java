package com.example.craigstevenson.storeloccraigs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by craigstevenson on 29/03/16.
 */
public class PlacesDatabaseHelper extends SQLiteOpenHelper {

    private static PlacesDatabaseHelper sInstance;

    private static final String DATABASE_NAME = "placesDatabase";
    private static final int DATABASE_VERSION = 4;

    private static final String TABLE_PLACES = "places";
    private static final String KEY_PLACE_ID = "placesID";
    private static final String KEY_PLACE_TEXT = "text";
    private static final String KEY_PLACE_NAME = "name";
    private static final String KEY_PLACE_LATITUDE = "latitude";
    private static final String KEY_PLACE_LONGITUDE = "longitude";
    private static final String KEY_PLACE_WEBSITE_URI = "website_uri";
    private static final String KEY_PLACE_PHONE_NUMBER = "phone_number";
    private static final String KEY_PLACE_PLACE_LAT_LNG = "place_lat_lng";
    private static final String KEY_PLACE_ADDRESS = "address";
    private static final String KEY_PLACE_GOOGLE_PLACE_ID = "Google_place_id";

    private static final String CREATE_PLACES_TABLE = "CREATE TABLE " + TABLE_PLACES + " (" +
            KEY_PLACE_ID                + " INTEGER PRIMARY KEY NOT NULL, " +
            KEY_PLACE_TEXT              + " TEXT, " +
            KEY_PLACE_NAME              + " TEXT, " +
            KEY_PLACE_LATITUDE          + " REAL, " +
            KEY_PLACE_LONGITUDE         + " REAL, " +
            KEY_PLACE_WEBSITE_URI       + " TEXT, " +
            KEY_PLACE_PHONE_NUMBER      + " TEXT, " +
            KEY_PLACE_PLACE_LAT_LNG     + " REAL, " +
            KEY_PLACE_ADDRESS           + " TEXT, " +
            KEY_PLACE_GOOGLE_PLACE_ID   + " TEXT" + ")";

    private static final String TAG = "PlacesDatabaseHelper";

    private PlacesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PLACES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);
            onCreate(db);
        }
    }

    // singleton design pattern
    public static synchronized PlacesDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PlacesDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public long save(Place place)
    {
        // Create and/or open the database for writing
        SQLiteDatabase db = getWritableDatabase();
        long placeID = 0;

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_PLACE_TEXT, place.getText());
            values.put(KEY_PLACE_NAME, place.getName());
            values.put(KEY_PLACE_LATITUDE, place.getLatitude());
            values.put(KEY_PLACE_LONGITUDE, place.getLongitude());
            values.put(KEY_PLACE_WEBSITE_URI, place.getWebsiteUri());
            values.put(KEY_PLACE_PHONE_NUMBER, place.getPhoneNumber());
            values.put(KEY_PLACE_PLACE_LAT_LNG, place.getPlaceLatLng());
            values.put(KEY_PLACE_ADDRESS, place.getAddress());
            values.put(KEY_PLACE_GOOGLE_PLACE_ID, place.getGooglePlaceID());

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            placeID = db.insertOrThrow(TABLE_PLACES, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add place to database");

        } finally {
            db.endTransaction();
        }

        return placeID;
    }

    public int update(Place place) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PLACE_TEXT, place.getText());

        // Updating text for place with that name
        return db.update(TABLE_PLACES, values, KEY_PLACE_NAME + " = ?",
                new String[] { String.valueOf(place.getName()) });
    }

    // get all places
    public List<Place> getAllPlaces() {
        List<Place> places = new ArrayList<>();

        String PLACES_SELECT_QUERY = String.format("SELECT * FROM %s", TABLE_PLACES);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(PLACES_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    Place newPlace = new Place();
                    newPlace.setText(cursor.getString(cursor.getColumnIndex(KEY_PLACE_TEXT)));
                    newPlace.setName(cursor.getString(cursor.getColumnIndex(KEY_PLACE_NAME)));
                    newPlace.setLatitude(cursor.getDouble(cursor.getColumnIndex(KEY_PLACE_LATITUDE)));
                    newPlace.setLongitude(cursor.getDouble(cursor.getColumnIndex(KEY_PLACE_LONGITUDE)));
                    newPlace.setWebsiteUri(cursor.getString(cursor.getColumnIndex(KEY_PLACE_WEBSITE_URI)));
                    newPlace.setPhoneNumber(cursor.getString(cursor.getColumnIndex(KEY_PLACE_PHONE_NUMBER)));
                    newPlace.setPlaceLatLng(cursor.getString(cursor.getColumnIndex(KEY_PLACE_PLACE_LAT_LNG)));
                    newPlace.setAddress(cursor.getString(cursor.getColumnIndex(KEY_PLACE_ADDRESS)));
                    newPlace.setGooglePlaceID(cursor.getString(cursor.getColumnIndex(KEY_PLACE_GOOGLE_PLACE_ID)));

                    places.add(newPlace);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get places from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return places;
    }

    public Place getByGooglePlacesID(String gPlaceID) {
        Place place = new Place();

        String PLACES_GET_BY_GOOGLEPLACEID_QUERY = String.format("SELECT * FROM " + TABLE_PLACES + " WHERE " + KEY_PLACE_GOOGLE_PLACE_ID + " = '%s'", gPlaceID);

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(PLACES_GET_BY_GOOGLEPLACEID_QUERY, null);

        try {
            if(cursor.moveToFirst()) {
                do {
                    place.setText(cursor.getString(cursor.getColumnIndex(KEY_PLACE_TEXT)));
                    place.setName(cursor.getString(cursor.getColumnIndex(KEY_PLACE_NAME)));
                    place.setLatitude(cursor.getDouble(cursor.getColumnIndex(KEY_PLACE_LATITUDE)));
                    place.setLongitude(cursor.getDouble(cursor.getColumnIndex(KEY_PLACE_LONGITUDE)));
                    place.setWebsiteUri(cursor.getString(cursor.getColumnIndex(KEY_PLACE_WEBSITE_URI)));
                    place.setPhoneNumber(cursor.getString(cursor.getColumnIndex(KEY_PLACE_PHONE_NUMBER)));
                    place.setPlaceLatLng(cursor.getString(cursor.getColumnIndex(KEY_PLACE_PLACE_LAT_LNG)));
                    place.setAddress(cursor.getString(cursor.getColumnIndex(KEY_PLACE_ADDRESS)));
                    place.setGooglePlaceID(cursor.getString(cursor.getColumnIndex(KEY_PLACE_GOOGLE_PLACE_ID)));
                } while(cursor.moveToNext());
            }
        } catch(Exception e) {
            Log.d(TAG, "Failed to load place from gPlaceID");
        }

        return place;
    }
}
