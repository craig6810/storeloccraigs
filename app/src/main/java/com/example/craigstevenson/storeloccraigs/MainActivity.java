package com.example.craigstevenson.storeloccraigs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startSessionActivity(View v) {
        Intent intent = new Intent(this, SessionActivity.class);
        startActivity(intent);
    }

    public void startHistoryActivity(View v) {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }
}
