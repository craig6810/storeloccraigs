package com.example.craigstevenson.storeloccraigs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by craigstevenson on 11/04/16.
 */
public class Place {
    public PlacesDatabaseHelper databaseHelper;
    private Context context;
    public static final String APP_FILE_PATH = "/data/data/com.example.craigstevenson.storeloccraigs/app_mainImage/";
    public static final String MAIN_IMAGE_LOCATION = "mainImage";
    private static final String TAG = "Place";
    private long placeID;
    private String text;
    private String name;
    private double latitude;
    private double longitude;
    private String websiteUri;
    private String phoneNumber;
    private String placeLatLng;
    private String address;
    private String GooglePlaceID;

    public Place(Context context) {
        this.context = context;
        this.databaseHelper = PlacesDatabaseHelper.getInstance(this.context);
    }

    public Place()
    {

    }

    public Place getByGPlaceID(String gPlaceID) {
        Place place = databaseHelper.getByGooglePlacesID(gPlaceID);

        return place;
    }

    public String getGooglePlaceID() {
        return GooglePlaceID;
    }

    protected Bitmap getMainImage() {
        Bitmap image = BitmapFactory.decodeFile( this.APP_FILE_PATH + this.getGooglePlaceID() + ".png");

        return image;
    }

    public long getPlaceID() { return placeID; }

    public int getDistance() {

        try {
            // lat/lng: (54.522913599999995,-6.083333199999998)
            String latLong = this.placeLatLng;

            // regular expression to extract lat and long
            String regex = "\\((-?\\d+\\.\\d+)\\,(-?\\d\\.\\d+)";

            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(latLong);

            m.find();
            String latitude = m.group(1);
            String longitude = m.group(2);

            Location placeLocation = new Location("place");
            placeLocation.setLatitude(Double.valueOf(latitude));
            placeLocation.setLongitude(Double.valueOf(longitude));

            Location userLocation = new Location("user location");
            userLocation.setLatitude(this.getLatitude());
            userLocation.setLongitude(this.getLongitude());

            int distance = (int) placeLocation.distanceTo(userLocation);

            return distance;
        }catch(Exception e){
            return 0;
        }
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getWebsiteUri() {
        return websiteUri;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPlaceLatLng() {
        return placeLatLng;
    }

    public String getAddress() {
        return address;
    }

    public String getText() {
        return text;
    }

    public void setDatabaseHelper(PlacesDatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setWebsiteUri(String websiteUri) {
        this.websiteUri = websiteUri;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPlaceLatLng(String placeLatLng) {
        this.placeLatLng = placeLatLng;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setGooglePlaceID(String googlePlaceID) {
        this.GooglePlaceID = googlePlaceID;
    }


    public String getName()
    {
        return name;
    }

    public void save() {

        try {
            this.placeID = databaseHelper.save(this);
        } catch( Exception e) {
            Log.d(TAG, "Failed to save: " + e.toString());
        }
    }

    public List<Place> getAll() {
        List<Place> places = databaseHelper.getAllPlaces();
        return places;
    }
}
