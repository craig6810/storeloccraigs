package com.example.craigstevenson.storeloccraigs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.places.Place;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SessionActivity extends FragmentActivity
        implements OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, ShakeEventManager.ShakeListener{

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private static final String TAG = "SessionActivity";
    int PLACE_PICKER_REQUEST = 1;
    public ImageView mMainImageView;
    public com.example.craigstevenson.storeloccraigs.Place place;
    private ShakeEventManager sd;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        mMainImageView = (ImageView) findViewById(R.id.placeMainImage);

        sd = new ShakeEventManager();
        sd.setListener(this);
        sd.init(this);

        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .enableAutoManage(this, this)
                .addApi(AppIndex.API).build();

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            if(!getIntent().hasExtra("gPlaceID"))
            {
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            } else {
                String gPlaceID = getIntent().getStringExtra("gPlaceID");

                com.example.craigstevenson.storeloccraigs.Place place = new com.example.craigstevenson.storeloccraigs.Place(this);
                final com.example.craigstevenson.storeloccraigs.Place newPlace = place.getByGPlaceID(gPlaceID);

                TextView title = (TextView) findViewById(R.id.titleText);
                title.setText(newPlace.getName());

                TextView uri = (TextView) findViewById(R.id.placeWebsiteURI);
                uri.setText(newPlace.getWebsiteUri());

                TextView telephoneNumber = (TextView) findViewById(R.id.placePhoneNumber);
                telephoneNumber.setText(newPlace.getPhoneNumber());

                TextView text = (TextView) findViewById(R.id.placeAddress);
                text.setText(newPlace.getAddress());

                Button getDirectionsButton = (Button) findViewById(R.id.getDirectionsButton);

                getDirectionsButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        // regular expression to extract lat and long
                        String regex = "\\((-?\\d+\\.\\d+)\\,(-?\\d\\.\\d+)";
                        Pattern p = Pattern.compile(regex);
                        Matcher m = p.matcher(newPlace.getPlaceLatLng());
                        m.find();
                        String latitude = m.group(1);
                        String longitude = m.group(2);

                        // Create a Uri from an intent string. Use the result to create an Intent.
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");

                        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        // Make the Intent explicit by setting the Google Maps package
                        mapIntent.setPackage("com.google.android.apps.maps");

                        // ensure user is capable of launching directions
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            // Attempt to start an activity that can handle the Intent
                            startActivity(mapIntent);
                        }
                    }
                });

                placePhotosTask(gPlaceID);
            }
        } catch (GooglePlayServicesRepairableException GooglePlayServicesRepairableError) {
            Log.d(TAG, "Error: " + GooglePlayServicesRepairableError.toString());
        } catch (GooglePlayServicesNotAvailableException GooglePlayServicesNotAvailableError) {
            Log.d(TAG, "Error: " + GooglePlayServicesNotAvailableError.toString());
        }
    }

    @Override
    public void onShake() {
        Intent intent = new Intent(this, MainActivity.class);
        finish(); // stop the current activity
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sd.register();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sd.deregister();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PLACE_PICKER_REQUEST) {
            if(resultCode == RESULT_OK) {
                Place gPlace = PlacePicker.getPlace(data, this);

                place = new com.example.craigstevenson.storeloccraigs.Place(this);

                try {
                    place.setName(gPlace.getName().toString());
                    place.setText(gPlace.getAddress().toString());
                    place.setLatitude(Double.valueOf(mLastLocation.getLatitude()));
                    place.setLongitude(Double.valueOf(mLastLocation.getLongitude()));
                    place.setWebsiteUri(String.valueOf(gPlace.getWebsiteUri()));
                    place.setPhoneNumber(String.valueOf(gPlace.getPhoneNumber()));
                    place.setPlaceLatLng(String.valueOf(gPlace.getLatLng()));
                    place.setAddress(String.valueOf(gPlace.getAddress()));
                    place.setGooglePlaceID(gPlace.getId());

                    place.save();
                } catch(Exception e)
                {
                    Log.d(TAG, "Failed to place");
                }

                TextView title = (TextView) findViewById(R.id.titleText);
                title.setText(gPlace.getName());

                TextView uri = (TextView) findViewById(R.id.placeWebsiteURI);
                uri.setText(place.getWebsiteUri());

                TextView telephoneNumber = (TextView) findViewById(R.id.placePhoneNumber);
                telephoneNumber.setText(place.getPhoneNumber());

                TextView text = (TextView) findViewById(R.id.placeAddress);
                text.setText(place.getAddress());

                Button getDirectionsButton = (Button) findViewById(R.id.getDirectionsButton);

                getDirectionsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // regular expression to extract lat and long
                        String regex = "\\((-?\\d+\\.\\d+)\\,(-?\\d\\.\\d+)";
                        Pattern p = Pattern.compile(regex);
                        Matcher m = p.matcher(place.getPlaceLatLng());
                        m.find();
                        String latitude = m.group(1);
                        String longitude = m.group(2);

                        // Create a Uri from an intent string. Use the result to create an Intent.
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude + "&mode=w");

                        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        // Make the Intent explicit by setting the Google Maps package
                        mapIntent.setPackage("com.google.android.apps.maps");

                        // ensure user is capable of launching directions
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            // Attempt to start an activity that can handle the Intent
                            startActivity(mapIntent);
                        }
                    }
                });

                // Use an asynk task to get the place image from the placeId
                placePhotosTask(gPlace.getId());
            }
        }
    }

    abstract class PhotoTask extends AsyncTask<String, Void, PhotoTask.AttributedPhoto> {
        private int mHeight;

        private int mWidth;

        public PhotoTask(int width, int height) {
            mHeight = height;
            mWidth = width;
        }

        @Override
        protected AttributedPhoto doInBackground(String... params) {
            if (params.length != 1) {
                return null;
            }
            final String placeId = params[0];
            AttributedPhoto attributedPhoto = null;

            PlacePhotoMetadataResult result = Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId).await();

            if (result.getStatus().isSuccess()) {
                PlacePhotoMetadataBuffer photoMetadata = result.getPhotoMetadata();
                if (photoMetadata.getCount() > 0 && !isCancelled()) {
                    // Get the first bitmap and its attributions.
                    PlacePhotoMetadata photo = photoMetadata.get(0);
                    // Load a scaled bitmap for this photo.
                    Bitmap image = photo.getScaledPhoto(mGoogleApiClient, mWidth, mHeight).await()
                            .getBitmap();

                    attributedPhoto = new AttributedPhoto(image);
                }
                // Release the PlacePhotoMetadataBuffer.
                photoMetadata.release();
            }
            return attributedPhoto;
        }

        /**
         * Holder for an image and its attribution.
         */
        class AttributedPhoto {

            public final Bitmap bitmap;

            public AttributedPhoto(Bitmap bitmap) {
                this.bitmap = bitmap;
            }
        }
    }


    private void placePhotosTask(final String gPlaceID) {
        // Create a new AsyncTask that displays the bitmaponce loaded
        // cannot get the image height and width when called from onCreate()
        // hard code for now
        new PhotoTask(720, 720) {
            @Override
            protected void onPreExecute() {
                // Display a temporary image to show while bitmap is loading.
                mMainImageView.setImageResource(R.drawable.loading);
            }

            @Override
            protected void onPostExecute(AttributedPhoto attributedPhoto) {
                if (attributedPhoto != null) {
                    // Photo has been loaded, display it.
                    Bitmap mainImageBitmap = attributedPhoto.bitmap;
                    mMainImageView.setImageBitmap(mainImageBitmap);

                    File internalStorage = mContext.getDir(place.MAIN_IMAGE_LOCATION, Context.MODE_PRIVATE);
                    File imageDirPath = new File(internalStorage, gPlaceID + ".png");

                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(imageDirPath);
                        mainImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                        // PNG is a lossless format, the compression factor (100) is ignored
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    mMainImageView.setImageResource(R.drawable.img_not_available);
                }
            }
        }.execute(gPlaceID);
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } catch (SecurityException e) {
            Log.d(TAG, "Security exception thrown");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Session Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.craigstevenson.storeloccraigs/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Session Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.craigstevenson.storeloccraigs/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
    }
}
