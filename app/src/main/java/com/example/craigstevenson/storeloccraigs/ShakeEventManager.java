package com.example.craigstevenson.storeloccraigs;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class ShakeEventManager implements SensorEventListener {

    Sensor s;
    SensorManager sManager;
    private static final float ALPHA = 0.8F;
    private static final int TIME_INTERVAL_FOR_SHAKE_LIMIT = 500; // milliseconds
    private static final int MAX_MOVE_COUNT = 4;
    private long firstMoveTime;
    private int counter;
    private static final int MOVE_THRESHOLD = 4;
    private ShakeListener listener;
    private float gravityForce[] = new float[3];

    public ShakeEventManager() {
    }

    public void setListener(ShakeListener listener) {
        this.listener = listener;
    }

    public void init(Context ctx) {
        sManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
        s = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        register();
    }

    public void register() {
        sManager.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float maxAcceleration = calcMaxAcceleration(event);
        Log.d("SwA", "Max Acc [" + maxAcceleration + "]");
        if (maxAcceleration >= MOVE_THRESHOLD) {
            if (counter == 0) {
                counter++;
                firstMoveTime = System.currentTimeMillis();
                Log.d("SwA", "First mov..");
            } else {
                long now = System.currentTimeMillis();
                if ((now - firstMoveTime) < TIME_INTERVAL_FOR_SHAKE_LIMIT) {
                    counter++;
                } else {
                    resetTimeData();
                    counter++;
                    return;
                }
                Log.d("SwA", "Mov counter [" + counter + "]");

                if (counter >= MAX_MOVE_COUNT) {
                    if (listener != null) {
                        listener.onShake();
                    }
                }
            }
        }
    }

    public void deregister() {
        sManager.unregisterListener(this);
    }

    private float calcMaxAcceleration(SensorEvent event) {
        gravityForce[0] = calcGravityForce(event.values[0], 0);
        gravityForce[1] = calcGravityForce(event.values[1], 1);
        gravityForce[2] = calcGravityForce(event.values[2], 2);

        float accX = event.values[0] - gravityForce[0];
        float accY = event.values[1] - gravityForce[1];
        float accZ = event.values[2] - gravityForce[2];

        float max1 = Math.max(accX, accY);
        return Math.max(max1, accZ);
    }

    // calculate the low pass filter
    private float calcGravityForce(float currentVal, int index) {
        return ALPHA * gravityForce[index] + (1 - ALPHA) * currentVal;
    }

    private void resetTimeData() {
        Log.d("SwA", "Reset all data");
        counter = 0;
        firstMoveTime = System.currentTimeMillis();
    }

    public interface ShakeListener {
        void onShake();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
